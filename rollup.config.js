import typescript from "@rollup/plugin-typescript";
import commonjs from "@rollup/plugin-commonjs";
import { nodeResolve } from "@rollup/plugin-node-resolve";

export default {
    input: "src/index.ts",
    output: {
        file: "build/index.js",
        sourcemap: "inline",
        format: "cjs",
        exports: "default"
    },
    watch: {
        include: "src/**"
    },
    external: [
        "@unh-csonline/discord-modules",
        "@discordjs/builders",
        "@discordjs/rest",
        "discord-api-types",
        "discord.js",
        "jsonschema",
        "mongoose"
    ],
    plugins: [
        typescript({
            tsconfig: "tsconfig.json"
        }),
        commonjs({
            ignore: ["supports-color"]
        }),
        nodeResolve({
            browser: false,
            preferBuiltins: false,
            exportConditions: ["node"]
        })
    ]
};
