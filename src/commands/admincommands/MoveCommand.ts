import { SlashCommandSubcommandBuilder } from "@discordjs/builders";
import {
    CategoryChannel,
    CommandInteraction,
    GuildMember,
    MessageActionRow,
    MessageSelectMenu,
    SelectMenuInteraction,
    Snowflake,
    TextChannel,
    VoiceChannel
} from "discord.js";
import { admin } from "subcommandGroups";
import { descriptionEmbed } from "utils/utils";
import { Subcommand } from "../Subcommand";

export class MoveCommand extends Subcommand {
    constructor(){
        super("move", admin);
    }

    buildSubcommand = function (subcommand: SlashCommandSubcommandBuilder) {
        return subcommand
            .setName("move")
            .setDescription("Move breakout room to a different category");
    }

    execute = async function (interaction: CommandInteraction): Promise<void> {
        if (!interaction.member) {
            await interaction
                .reply({
                    embeds: [
                        descriptionEmbed("Command can only be used in a guild")
                    ],
                    ephemeral: true
                })
                .catch(console.error);
            return;
        }

        if (!(interaction.member as GuildMember).permissions.has("ADMINISTRATOR")) {
            await interaction
                .reply({
                    embeds: [descriptionEmbed("Only admins can use this command")],
                    ephemeral: true
                })
                .catch(console.error);
            return;
        }

        const textChannel = interaction.channel as TextChannel;
        if (!textChannel.name.startsWith("breakout")) {
            await interaction.reply({
                embeds: [descriptionEmbed("Command can only be used in breakout")],
                ephemeral: true
            });
            return;
        }

        const voiceChannel = textChannel.guild.channels.cache.find(
            (ch) =>
                ch.parentId === textChannel.parentId &&
                ch.name === textChannel.name &&
                ch.type === "GUILD_VOICE"
        );

        const parentName = textChannel.parent ? textChannel.parent.name : "";

        const channels = await interaction.guild?.channels
            .fetch()
            .catch(console.error);
        const categories = channels?.filter(
            (c) => c !== null && c.type === "GUILD_CATEGORY" && c.name !== parentName
        );

        const menu = new MessageSelectMenu().setCustomId("Categories");
        if (parentName !== "")
            menu.addOptions([{ label: "/", description: "", value: "null" }]);
        categories?.forEach((c) => {
            if (c) menu.addOptions([
                { label: `/${c.name}`, description: "", value: c.id }
            ]);
        });
 
        const row = new MessageActionRow().addComponents(menu);

        await interaction
            .reply({
                ephemeral: true,
                components: [row],
                embeds: [descriptionEmbed("Select category")]
            })
            .catch(console.error);

        const choice = await textChannel
            .awaitMessageComponent({
                filter: (i) => {
                    return i.user.id === interaction.user.id;
                },
                time: 120000
            })
            .catch(console.error);
        if (!choice) {
            await interaction
                .followUp({
                    ephemeral: true,
                    embeds: [
                        descriptionEmbed(
                            `Timeout occurred while waiting for category selection.
                        Please re-execute the \`/move\` command if you wish to try again.`
                        )
                    ]
                })
                .catch(console.error);
            return;
        }
        await choice.deferReply({ ephemeral: true }).catch(console.error);

        let sf: Snowflake | null = (choice as SelectMenuInteraction).values[0];
        let spaceNeeded = 48;
        if (!voiceChannel) spaceNeeded++;

        if (sf === "null") {
            sf = null;
        } else {
            const cat = categories?.get(sf);
            if (cat && (cat as CategoryChannel).children.size > spaceNeeded) {
                await choice
                    .followUp({
                        ephemeral: true,
                        embeds: [
                            descriptionEmbed(
                                "Target category contains too many channels to move more into"
                            )
                        ]
                    })
                    .catch(console.error);
                return;
            }
        }
        await textChannel
            .setParent(sf, { lockPermissions: false })
            .catch(() =>
                choice.followUp({
                    ephemeral: true,
                    embeds: [descriptionEmbed("Text channel could not moved")]
                })
            )
            .catch(console.error);

        if (voiceChannel) {
            await (voiceChannel as VoiceChannel)
                .setParent(sf, { lockPermissions: false })
                .catch(() => {
                    choice
                        .followUp({
                            ephemeral: true,
                            embeds: [
                                descriptionEmbed("Voice channel could not moved")
                            ]
                        })
                        .catch(console.error);
                });
        }

        await choice
            .followUp({
                ephemeral: true,
                embeds: [descriptionEmbed("Command finished")]
            })
            .catch(console.error);

        return;
    }
}