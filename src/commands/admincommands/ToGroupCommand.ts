import { SlashCommandSubcommandBuilder } from "@discordjs/builders";
import { CommandInteraction, GuildMember } from "discord.js";
import { admin } from "subcommandGroups";
import { descriptionEmbed } from "utils/utils";
import { Subcommand } from "../Subcommand";

export class ToGroupCommand extends Subcommand {
    constructor() {
        super("togroup", admin);
    }

    buildSubcommand = function (subcommand: SlashCommandSubcommandBuilder) {
        return subcommand
            .setName("togroup")
            .setDescription("Change 'breakout' prefix to 'group'");
    };

    execute = async function (interaction: CommandInteraction): Promise<void> {
        let newName = "";

        if (!interaction.guild) {
            await interaction
                .reply({
                    ephemeral: true,
                    embeds: [
                        descriptionEmbed("ERROR: command must be used in guild")
                    ]
                })
                .catch(console.error);
            return;
        }

        if (
            interaction.channel?.type !== "GUILD_TEXT" ||
            !interaction.channel.name.startsWith("breakout")
        ) {
            await interaction
                .reply({
                    ephemeral: true,
                    embeds: [
                        descriptionEmbed(
                            "ERROR: command must be used in breakout room"
                        )
                    ]
                })
                .catch(console.error);
            return;
        }

        const channel = interaction.channel;

        //command useable by admins and room creator, breaks if topic paradigm is changed
        if (
            !channel.topic?.includes(
                `@${(interaction.member as GuildMember).displayName}`
            ) &&
            !(interaction.member as GuildMember).permissions.has(
                "ADMINISTRATOR"
            )
        ) {
            await interaction
                .reply({
                    ephemeral: true,
                    embeds: [
                        descriptionEmbed(
                            "ERROR: you do not have permission to use this command in this room.\n" +
                                "If you believe this is a mistake, please contact an administrator"
                        )
                    ]
                })
                .catch(console.error);
            return;
        }

        newName = "group-" + channel.name.slice(8);
        newName = newName.replaceAll(/-+/g, "-");

        //find paired voice channel
        const pair = channel.guild.channels.cache.find(
            (ch) =>
                ch.parentId === channel.parentId &&
                ch.name === channel.name &&
                ch.type === "GUILD_VOICE"
        );

        await channel.edit({ name: newName });
        await pair?.edit({ name: newName });

        await interaction
            .reply({
                ephemeral: true,
                embeds: [
                    descriptionEmbed(
                        `Breakout channel name has been set to "${newName}"`
                    )
                ]
            })
            .catch(console.error);

        return;
    };
}
