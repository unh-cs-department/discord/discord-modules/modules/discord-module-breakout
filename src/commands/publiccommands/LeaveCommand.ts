import { SlashCommandSubcommandBuilder } from "@discordjs/builders";
import {
    CommandInteraction,
    GuildChannel,
    GuildMember,
    PermissionOverwriteOptions,
    RoleResolvable,
    TextChannel,
    UserResolvable,
    VoiceChannel
} from "discord.js";
import { descriptionEmbed } from "utils/utils";
import { Subcommand } from "../Subcommand";

export class LeaveCommand extends Subcommand {
    constructor(){
        super("leave");
    }

    buildSubcommand = function (subcommand: SlashCommandSubcommandBuilder) {
        return subcommand.setName("leave").setDescription("Leave a breakout room.");
    }

    execute = async function (interaction: CommandInteraction): Promise<void> {
        if (interaction.channel?.type !== "GUILD_TEXT") return;

        // receives text and voice channels to remove.
        const textChannel: TextChannel = interaction.channel;
        const voiceChannel: VoiceChannel | undefined =
            textChannel.guild.channels.cache.find(
                (ch) => ch.name === textChannel.name && ch.type === "GUILD_VOICE"
            ) as VoiceChannel | undefined;

        // cancels leave if not in breakout.
        if (!textChannel.name.startsWith("breakout")) {
            await interaction
                .reply({
                    embeds: [
                        descriptionEmbed(
                            "Command can only be used in a breakout room"
                        )
                    ],
                    ephemeral: true
                })
                .catch(console.error);
            return;
        }

        const member = interaction.member as GuildMember;

        // update permissions to remove leaving member.
        await updateOverwrite(textChannel, member, {
            VIEW_CHANNEL: false,
            SEND_MESSAGES: false
        }).catch(console.error);

        if (voiceChannel)
            await updateOverwrite(voiceChannel, member, {
                VIEW_CHANNEL: false
            }).catch(console.error);

        await interaction
            .reply({
                embeds: [
                    descriptionEmbed(
                        `@${member.displayName} has left the breakout.`
                    )
                ]
            })
            .catch(console.error);

        let bitfieldTotal = 0n;

        const members = textChannel.permissionOverwrites.cache
            .filter((po) => po.type === "member" && !po.allow.bitfield)
            .map((m) => m.id);

        console.log(`exporting permissions for '${textChannel.name}'`);

        //const fetchedChannel = await textChannel.fetch(true);
        const permissionOverwrites = textChannel.permissionOverwrites.cache.map(
            (po) => po
        );

        const guild = textChannel.guild;

        // remove role if all users have left.
        for (const permissionOverwrite of permissionOverwrites) {
            console.log(
                `${permissionOverwrite.type}, ${permissionOverwrite.id}, ALLOW, ${permissionOverwrite.allow.bitfield}`
            );
            console.log(
                `${permissionOverwrite.type}, ${permissionOverwrite.id}, DENY,  ${permissionOverwrite.deny.bitfield}`
            );

            if (permissionOverwrite.type === "role") {
                const role = guild.roles.cache.find(
                    (r) => r.id === permissionOverwrite.id
                );

                // if all members presently in the role have left the channel, remove the role.
                if (role && role.members.every((m) => members.includes(m.id))) {
                    await permissionOverwrite
                        .edit({ VIEW_CHANNEL: false, SEND_MESSAGES: false })
                        .catch(console.error);
                    continue;
                }
            }

            // if the overwrite is a user a role that still contains present users, add to bitfield total.
            bitfieldTotal += permissionOverwrite.allow.bitfield;
        }

        console.log(`bitfieldTotal: ${bitfieldTotal}`);

        // remove breakout if no one has permission
        if (!bitfieldTotal) {
            await deleteBreakout(textChannel, voiceChannel).catch(console.error);
        }

        return;
    }
}

// helper function for updating permission overwrites.
async function updateOverwrite(
    channel: GuildChannel,
    roleOrMember: UserResolvable | RoleResolvable,
    permissions: PermissionOverwriteOptions
) {
    await channel.permissionOverwrites
        .edit(roleOrMember, permissions)
        .then(() =>
            console.log(`[ops] overwrote permission for '${channel.name}'`)
        )
        .catch(console.error);
}

// helper function for deleting a breakout.
async function deleteBreakout(
    textChannel: TextChannel,
    voiceChannel?: VoiceChannel
) {
    await Promise.all([textChannel.delete(), voiceChannel?.delete()])
        .then((res) =>
            console.log(
                `[ops] deleted breakout '${(res[0] as TextChannel).name}'`
            )
        )
        .catch(console.error);
}
