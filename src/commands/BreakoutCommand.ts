import { SlashCommandBuilder } from "@discordjs/builders";
import {
    GuildApplicationCommand,
    GuildApplicationCommandOptions,
    Module
} from "@unh-csonline/discord-modules";
import { CommandInteraction } from "discord.js";

import { BreakoutCreate } from "./publiccommands/BreakoutCreateCommand";
import { AddCommand } from "./publiccommands/AddCommand";
import { RenameCommand } from "./publiccommands/RenameCommand";
import { LeaveCommand } from "./publiccommands/LeaveCommand";
import { AdminCreateCommand } from "./admincommands/AdminCreateCommand";
import { CloseCommand } from "./admincommands/CloseCommand";
import { MoveCommand } from "./admincommands/MoveCommand";

import { ToGroupCommand } from "./admincommands/ToGroupCommand";
import { Subcommand, SubcommandGroup } from "commands/Subcommand";

const commands: Map<string, Subcommand> = new Map<string, Subcommand>();
commands.set("create", new BreakoutCreate());
commands.set("add", new AddCommand());
commands.set("rename", new RenameCommand());
commands.set("leave", new LeaveCommand());
commands.set("createmany", new AdminCreateCommand());
commands.set("close", new CloseCommand());
commands.set("move", new MoveCommand());
commands.set("togroup", new ToGroupCommand());

export class BreakoutCommand extends GuildApplicationCommand {
    constructor(module: Module) {
        const data = new SlashCommandBuilder()
            .setName("breakout")
            .setDescription("Perform breakout room related operations");

        //Tracks all the SubcommandGroups to their associated SubCommands
        const groups: Map<SubcommandGroup, Subcommand[]> = new Map<
            SubcommandGroup,
            Subcommand[]
        >();

        //add all subcommands without a group to the builder, and sort all subcommands with a group into groups
        commands.forEach((cmd) => {
            if (!cmd.group) {
                data.addSubcommand(cmd.buildSubcommand);
            } else {
                const group = groups.get(cmd.group);
                if (group) {
                    group.push(cmd);
                } else {
                    groups.set(cmd.group, [cmd]);
                }
            }
        });

        //for every subcommand group
        groups.forEach((commands, group) => {
            data.addSubcommandGroup((subCommandGroup) => {
                //add the groups name and description to the builder
                subCommandGroup.setName(group.name);
                subCommandGroup.setDescription(group.description);

                //add every subcommand withing the group to the builder
                commands.forEach((cmd) => {
                    subCommandGroup.addSubcommand(cmd.buildSubcommand);
                });

                return subCommandGroup;
            });
        });

        super(module, {
            guild: module.client.guilds.cache.get(
                process.env.GUILD_ID as string
            ),
            data: data
        } as GuildApplicationCommandOptions);
    }

    public async execute(interaction: CommandInteraction): Promise<void> {
        const command = commands.get(interaction.options.getSubcommand());

        command?.execute(interaction).catch(console.error);

        if (!command) console.error("received unregistered subcommand");

        return;
    }
}
