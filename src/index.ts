import { Client, Module, ModuleManifest } from "@unh-csonline/discord-modules";
import { BreakoutCommand } from "./commands/BreakoutCommand";

export default class BreakoutsModule extends Module {
    constructor(client: Client, manifest: ModuleManifest) {
        super(client, manifest);

        console.info("INFO: Constructed discord-modules-breakouts.");
    }

    public async register(): Promise<void> {
        console.info("INFO: Registered discord-modules-breakouts.");

        await Promise.all([
            this.client.guildCommandManager.register(new BreakoutCommand(this))
            //this.client.guildCommandManager.register(new BreakoutSettingsCommand(this)),
        ]).catch(console.error);

        return;
    }
}
